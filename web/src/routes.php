<?php

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;

return function (App $app) {		
		$app->get('/cors-proba', function (Request $request, Response $response, array $args) {
			$response = $response->withJson('{success: true}');
			$response->withHeader('Content-type', 'application/json');
			return $response;
		});
};
